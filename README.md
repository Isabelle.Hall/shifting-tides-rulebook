# shifting_tides_rulebook

This application is designed to replace the PDF of my TTRPG, Shifting Tides. It will have a built-in character sheet + Character creator, as well as the rule book itself + weapon generator (for DM's).
There's possible plans for expanding the app down the road to include map generators and other fun stuff.