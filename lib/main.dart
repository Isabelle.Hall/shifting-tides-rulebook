import 'package:flutter/material.dart';
import 'package:get/get.dart';
import 'file:///C:/Users/Isabelle/AndroidStudioProjects/shifting_tides_rulebook/lib/Views/Equipment/create_New_Equipment.dart';
import 'package:shifting_tides_rulebook/Views/homepage.dart';
import 'Views/character_Sheet.dart';

void main() {
  runApp(GetMaterialApp(
    initialRoute: '/Homepage',
    getPages: [
      GetPage(name: '/Homepage', page: () => HomePage()),
      GetPage(name: '/Character_Sheet', page: () => CharacterSheet()),
      GetPage(name: '/Equipment_Sheet', page: () => EquipmentSheet())
    ],
  ));
}
