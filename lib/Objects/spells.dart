import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';

class Spell {
  String name;
  int tier;
  int manaCost;
  int fracturePointCost;
  int naniteCost;
  int range;
  var duration;
  int radius;
  List<String> tags;
  String description;

  Spell(this.description, this.tier, this.duration, this.name, this.fracturePointCost, this.manaCost, this.naniteCost, this.radius, this.range, this.tags);

  Widget render({Function onTap, Widget trailing}) {
      return Container(
        decoration: BoxDecoration(color: CupertinoColors.white),
        child: ListTile(
          leading: CircleAvatar(),
          title: Text(this.name),
          subtitle: Container(
              child: Column(
            children: [
              Text(this.tier.toString()),
              Text(this.description),
              Divider(
                thickness: 1,
              ),
              Row(
                children: [
                  ListView.builder(
                    itemCount: this.tags.length,
                    itemBuilder: (BuildContext context, int index) {
                      return Text(this.tags[index]);
                    },
                  ),
                ],
              ),
              Divider(
                thickness: 1,
              ),
              Row(
                children: [
                  Text("Mana Cost: " + this.manaCost.toString() + " | "),
                  Text("Nanite Cost: " + this.naniteCost.toString() + " | "),
                  Text("Fracture Point Upkeep: " + this.fracturePointCost.toString() + " | "),
                  Text("Duration: " + this.description + " | "),
                  Text("Range: " + this.range.toString() + " | "),
                  Text("Radius: " + this.radius.toString()),
                ],
              ),
            ],
          )),
          onTap: onTap,
        ));
  }
}
