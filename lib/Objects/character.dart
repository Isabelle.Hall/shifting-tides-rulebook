import 'dart:core';
import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';
import 'package:shifting_tides_rulebook/Objects/spells.dart';

class Character {
  String name = "";
  int health = 0;
  int age = 0;
  int mana = 0;
  int nanites = 0;
  int fracturePoints = 0;
  int woundSlots = 4;
  int tier = 1;
  int baseSuccessDice = 3;
  Attributes attributes;
  Species species;
  Class characterClass;
  Skills skills;

  bool majorAttributeChosen = false;
  bool attributesChosen = false;
  bool subAttributesChosen = false;

  List<Features> talent;
  List equipment;
  List<Spell> spells;

  Character(this.name, this.health, this.age, this.mana, this.nanites, this.fracturePoints, this.woundSlots, this.tier, this.baseSuccessDice, this.talent, this.attributes, this.species, this.characterClass,
      this.skills, this.equipment, this.spells);
}

class Attributes {
  int body = 0;
  int strength = 0;
  int agility = 0;
  int finesse = 0;

  int mind = 0;
  int precision = 0;
  int calculation = 0;
  int intuition = 0;

  int spirit = 0;
  int resonance = 0;
  int projection = 0;
  int poise = 0;

  Attributes(this.body, this.agility, this.calculation, this.finesse, this.intuition, this.mind, this.poise, this.precision, this.projection, this.resonance, this.spirit, this.strength);
}

class Skills {
  int conceal = 0;

  Skills(this.conceal, this.climb, this.slightOfHand, this.acrobatics, this.lockPicking, this.metalworking, this.alchemy, this.artificing, this.medical, this.metaphysics, this.metapsionics,
      this.ancient, this.lore, this.nature, this.tracking, this.combatAwareness, this.linguistics, this.melee, this.bow, this.thrown, this.ranged);

  int climb = 0;
  int slightOfHand = 0;
  int acrobatics = 0;
  int lockPicking = 0;
  int metalworking = 0;
  int alchemy = 0;
  int artificing = 0;

  int medical = 0;
  int metaphysics = 0;
  int metapsionics = 0;
  int ancient = 0;

  List<int> lore = [];
  int nature = 0;
  int tracking = 0;
  int combatAwareness = 0;
  int linguistics = 0;

  var melee = {'fist': 0, 'light': 0, 'medium': 0, 'heavy': 0, 'ancient': 0, 'unarmed': 0};
  int bow = 0;
  int thrown = 0;
  var ranged = {
    'light': 0,
    'medium': 0,
    'heavy': 0,
  };
}

class Species {
  String name = "";
  String description = "";
  List<Features> features;

  Species(this.description, this.name, this.features);
}

class Features {
  String name;
  String description;
  Features(this.description, this.name);

  Widget render({Function onTap, Widget trailing}) {
    return Container(
        decoration: BoxDecoration(color: CupertinoColors.white),
        child: ListTile(
          title: Text(this.name),
          subtitle: Text(this.description, style: TextStyle(fontSize: 12)),
          onTap: onTap,
        ));
  }
}

class Class {
  String type;
  List<Features> features;
  Class(this.features, this.type);
}
