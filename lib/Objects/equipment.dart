import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';
import 'package:shifting_tides_rulebook/Objects/character.dart';

class Equipment {
  int weight;
  String name;
  String description;
  int price;
  Equipment(this.weight, this.name, this.description, this.price);

  Widget render({Function onTap, Widget trailing}) {
    return Padding(
      padding: const EdgeInsets.symmetric(
        horizontal: 16,
      ),
      child: Container(
        decoration: BoxDecoration(color: CupertinoColors.white),
        child: ListTile(
          leading: CircleAvatar(),
          title: Text(this.name, style: TextStyle(fontSize: 12)),
          subtitle: Text(this.description, style: TextStyle(fontSize: 12)),
          trailing: trailing ??
              Padding(
                padding: const EdgeInsets.all(12.0),
                child: Icon(Icons.chevron_right),
              ),
          onTap: onTap,
        ),
      ),
    );
  }
}

class Stimulants {
  String name;
  String description;
  int duration;
  int tier;
  int weight;

  Stimulants(this.name, this.description, this.duration, this.tier, this.weight);

  Widget render({Function onTap, Widget trailing}) {
    return Padding(
      padding: const EdgeInsets.symmetric(
        horizontal: 16,
      ),
      child: Container(
        decoration: BoxDecoration(color: CupertinoColors.white),
        child: ListTile(
          leading: CircleAvatar(),
          title: Text(this.name, style: TextStyle(fontSize: 12)),
          subtitle: Text(this.description, style: TextStyle(fontSize: 12)),
          trailing: trailing ??
              Padding(
                padding: const EdgeInsets.all(12.0),
                child: Icon(Icons.chevron_right),
              ),
          onTap: onTap,
        ),
      ),
    );
  }
}

class Weapon {
  String damage;
  String damageType;
  String attributeType;
  String type;
  String description;
  int sockets;
  int rarity;
  int price;
  int weight;
  Color rarityColor;
  Features talent;
  Modifiers modifiers;


  Weapon(this.damage, this.damageType, this.attributeType, this.type, this.description, this.sockets, this.rarity, this.price, this.weight, this.rarityColor, this.talent, this.modifiers);

  Widget render({Function onTap, Widget trailing}) {
    return Card(
      child: Container(
        color: this.rarityColor,
        child: Card(
          child: ListTile(
            leading: CircleAvatar(),
            title: Row(
              children: [
                Text(this.attributeType + " - " + this.type, style: TextStyle(fontSize: 12)),
                Spacer(),
                Text(this.damage + " Physical", style: TextStyle(fontSize: 12)),
              ],
            ),
            subtitle: Column(
              children: [
                (this.description == "") ? Text("This weapon has no description") : Text(this.description),
                if (this.modifiers.prefix.length == 0)
                  Text("This item has no prefixes", style: TextStyle(fontSize: 12))
                else
                  ListView.builder(
                    scrollDirection: Axis.vertical,
                    physics: NeverScrollableScrollPhysics(),
                    shrinkWrap: true,
                    itemCount: this.modifiers.prefix.length,
                    itemBuilder: (BuildContext context, int index) {
                      return Row(
                        mainAxisAlignment: MainAxisAlignment.center,
                        children: [
                          Text(this.modifiers.prefix[index], textAlign: TextAlign.center, style: TextStyle(fontSize: 12)),
                        ],
                      );
                    },
                  ),
                if (this.modifiers.suffix.length == 0)
                  Text("This item has no suffixes", style: TextStyle(fontSize: 12))
                else
                  ListView.builder(
                    scrollDirection: Axis.vertical,
                    physics: NeverScrollableScrollPhysics(),
                    shrinkWrap: true,
                    itemCount: this.modifiers.suffix.length,
                    itemBuilder: (BuildContext context, int index) {
                      return Row(
                        mainAxisAlignment: MainAxisAlignment.center,
                        children: [
                          Text(this.modifiers.suffix[index], textAlign: TextAlign.center, style: TextStyle(fontSize: 12)),
                        ],
                      );
                    },
                  )
              ],
            ),
            onTap: onTap,
          ),
        ),
      ),
    );
  }
}

class Modifiers {
  List<String> prefix;
  List<String> suffix;
  Modifiers(this.prefix, this.suffix);
}

class Armour{
  String armourSize = "Light";
  String name;
  String description;
  var defence = [0, 0, 0];
  var thresholdDefence = [0, 0, 0];
  int price;
  int concealPenalty = 0;
  int movementPenalty = 0;
  int pocketSpace = 0;
  int successRating = 0;
  int armourType = 1;
  int weight;

  Armour(this.armourSize, this.name, this.description, this.defence, this.thresholdDefence, this.price, this.concealPenalty, this.movementPenalty, this.pocketSpace, this.successRating,
      this.armourType, this.weight);

  Widget render({Function onTap, Widget trailing}) {
    return Padding(
      padding: const EdgeInsets.symmetric(
        horizontal: 16,
      ),
      child: Container(
        decoration: BoxDecoration(color: CupertinoColors.white),
        child: ListTile(
          leading: CircleAvatar(),
          title: Column(children: [Row(children: [Text(this.name, style: TextStyle(fontSize: 12)) ?? Text('', style: TextStyle(fontSize: 12))],), Row(children: [Text(this.armourSize + " armour",
              style: TextStyle(fontSize: 12))],)],),
          subtitle: Container(
              child: Column(
            children: [
              Text(this.description),
              Divider(
                thickness: 2,
              ),
              Text("Defence: " + this.defence[0].toString() + " | " + this.defence[1].toString() + " | " + this.defence[2].toString() + " | ", style: TextStyle(fontSize: 12)),
              Text("Threshold Defence: " + this.thresholdDefence[0].toString() + " | " + this.thresholdDefence[1].toString() + " | " + this.thresholdDefence[2].toString() + " | ",
                  style: TextStyle(fontSize: 12)),
              Column(
                mainAxisAlignment: MainAxisAlignment.spaceEvenly,
                children: [
                  Row(
                    mainAxisAlignment: MainAxisAlignment.spaceEvenly,
                    children: [
                      Text("TNoS: " + this.successRating.toString(), style: TextStyle(fontSize: 12)),
                      Text("Conceal Penalty: " + this.concealPenalty.toString(), style: TextStyle(fontSize: 12)),
                      Text("Pocket Space: " + this.pocketSpace.toString(), style: TextStyle(fontSize: 12)),
                    ],
                  ),
                ],
              ),
              Divider(
                thickness: 1,
              ),
              Text("Price: " + this.price.toString(), style: TextStyle(fontSize: 12))
            ],
          )),
          onTap: onTap,
        ),
      ),
    );
  }
}

class Shield {
  String name;
  String description;
  int price;
  int weight;
  var defenceMod = [0, 0, 0];
  var thresholdDefence = [0, 0, 0];

  Shield(this.name, this.description, this.price, this.weight, this.defenceMod, this.thresholdDefence);

  Widget render({Function onTap, Widget trailing}) {
    return Padding(
      padding: const EdgeInsets.symmetric(
        horizontal: 16,
      ),
      child: Container(
        decoration: BoxDecoration(color: CupertinoColors.white),
        child: ListTile(
          leading: CircleAvatar(),
          title: Text(this.name, style: TextStyle(fontSize: 12)),
          subtitle: Container(
              child: Column(
            children: [
              Text(this.description),
              Divider(
                thickness: 2,
              ),
              Text("Defence: " + this.defenceMod[1].toString() + " | " + this.defenceMod[2].toString() + " | " + this.defenceMod[3].toString() + " | ", style: TextStyle(fontSize: 12)),
              Text("Threshold Defence: " + this.thresholdDefence[1].toString() + " | " + this.thresholdDefence[2].toString() + " | " + this.thresholdDefence[3].toString() + " | ",
                  style: TextStyle(fontSize: 12)),
              Divider(
                thickness: 1,
              ),
              Text("Price: " + this.price.toString(), style: TextStyle(fontSize: 12))
            ],
          )),
          onTap: onTap,
        ),
      ),
    );
  }
}
