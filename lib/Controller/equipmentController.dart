import 'package:flutter/material.dart';
import 'package:shifting_tides_rulebook/Objects/character.dart';
import 'package:shifting_tides_rulebook/Objects/equipment.dart';


class EquipmentController
{
  createEquipment()
  {

  }

  updateEquipment()
  {

  }

  deleteEquipment()
  {

  }

  getEquipment()
  {

  }

  Weapon createWeapon()
  {
    Features talent = new Features("Test - description", "Test");
    Modifiers modifiers = new Modifiers([], []);
    Weapon _weapon = new Weapon("3d4", "Physical", "Strength", "Bladed", "", 0, 1, 0, 0, Colors.red[200], talent, modifiers);
    return _weapon;
  }

  Armour createArmour()
  {
    Armour template = new Armour("", "", "", [0,0,0], [0,0,0], 0, 0, 0, 0, 0, 1, 0);
    return template;
  }

  getWeaponModifiers()
  {

  }

  addWeaponModifiers()
  {


  }

  removeWeaponModifiers()
  {

  }

  List<Armour> listArmours()
  {
    List<Armour> armourList = [];
    //Armour Template
    //Armour template = new Armour("", "", "", [0,0,0], [0,0,0], 0, 0, 0, 0, 0, 1, 0);
    //Light Armours
    Armour light1 = new Armour("Light", "Fort Redux Armour Vest", "", [0,0,0], [0,0,0], 0, 0, 0, 0, 0, 1, 0);
    Armour light2 = new Armour("Light", "PhaseLock Light Mobility Shell", "", [0,0,0], [0,0,0], 0, 0, 0, 0, 0, 1, 0);
    Armour light3 = new Armour("Light", "HSV Nano-Mesh", "", [0,0,0], [0,0,0], 0, 0, 0, 0, 0, 1, 0);
    Armour light4 = new Armour("Light", "Jaeger Recon Armour", "", [0,0,0], [0,0,0], 0, 0, 0, 0, 0, 1, 0);
    Armour light5 = new Armour("Light", "Avalon Lite-PanelFrame", "", [0,0,0], [0,0,0], 0, 0, 0, 0, 0, 1, 0);
    armourList.add(light1);
    armourList.add(light2);
    armourList.add(light3);
    armourList.add(light4);
    armourList.add(light5);
    //Medium Armour
    Armour medium1 = new Armour("Medium", "L&J Ballistic Gen4", "", [0,0,0], [0,0,0], 0, 0, 0, 0, 0, 2, 0);
    Armour medium2 = new Armour("Medium", "Shinkawa Assault Armour", "", [0,0,0], [0,0,0], 0, 0, 0, 0, 0, 2, 0);
    Armour medium3 = new Armour("Medium", "Hexgrid Ceramic Plate", "", [0,0,0], [0,0,0], 0, 0, 0, 0, 0, 2, 0);
    Armour medium4 = new Armour("Medium", "Psion Legionnaire Armour", "", [0,0,0], [0,0,0], 0, 0, 0, 0, 0, 2, 0);
    Armour medium5 = new Armour("Medium", "Avalon Cell-Mesh", "", [0,0,0], [0,0,0], 0, 0, 0, 0, 0, 2, 0);
    armourList.add(medium1);
    armourList.add(medium2);
    armourList.add(medium3);
    armourList.add(medium4);
    armourList.add(medium5);
    //Heavy Armours
    Armour heavy1 = new Armour("Heavy", "Shinkawa Plate Suit", "", [0,0,0], [0,0,0], 0, 0, 0, 0, 0, 3, 0);
    Armour heavy2 = new Armour("Heavy", "Starfield Exo-suit", "", [0,0,0], [0,0,0], 0, 0, 0, 0, 0, 3, 0);
    Armour heavy3 = new Armour("Heavy", "Fort Venturi Armour", "", [0,0,0], [0,0,0], 0, 0, 0, 0, 0, 3, 0);
    Armour heavy4 = new Armour("Heavy", "PhaseLock Heavy Mobility Armour", "", [0,0,0], [0,0,0], 0, 0, 0, 0, 0, 3, 0);
    Armour heavy5 = new Armour("Heavy", "Guardian Powered PlateGuardian Powered Plate", "", [0,0,0], [0,0,0], 0, 0, 0, 0, 0, 3, 0);
    armourList.add(heavy1);
    armourList.add(heavy2);
    armourList.add(heavy3);
    armourList.add(heavy4);
    armourList.add(heavy5);

    return armourList;
  }
}

