import 'package:shifting_tides_rulebook/Objects/character.dart';

class CharacterController
{
  Character createCharacter()
  {
    Attributes attributes = Attributes(0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0);
    Species species = Species('', '', []);
    Class characterClass = Class([], "");
    Skills skills = Skills(0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, [], 0, 0, 0, 0, {}, 0, 0, {});
    Character _character = Character('', 0, 0, 0, 0, 0, 4, 1, 3, [], attributes, species, characterClass, skills, [], []);

    return _character;
  }

  listCharacters()
  {

  }

  updateCharacter()
  {

  }

  deleteCharacter()
  {

  }

// Character Modifiers //

  getCharacterModifier()
  {

  }

// Character skills //

  getCharacterSkills()
  {

  }

  modifyCharacterSkills()
  {

  }
}
