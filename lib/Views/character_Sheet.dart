import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';
import 'package:get/get_core/src/get_main.dart';
import 'package:get/get.dart';
import 'package:shifting_tides_rulebook/Objects/character.dart';

class CharacterSheet extends StatefulWidget {
  CharacterSheet() : super();

  _CharacterSheetState createState() => _CharacterSheetState();
}

class _CharacterSheetState extends State<CharacterSheet> with SingleTickerProviderStateMixin {
  Character character = Get.arguments;
  int selectedValue = 1;
  int itemSelection = 1;

  @override
  void initState() {
    setState(() {

    });
    super.initState();
  }

  @override
  void dispose() {
    super.dispose();
  }

  SliverAppBar showSliverAppBar(String screenTitle) {
    return SliverAppBar(
      backgroundColor: Colors.purple,
      floating: true,
      pinned: true,
      snap: false,
      title: Text(screenTitle),
      bottom: TabBar(
        tabs: [
          Tab(text: "Stats"),
          Tab(text: "Equipment"),
          Tab(text: "Features"),
          Tab(text: "Spells"),
        ],
      ),
    );
  }

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      backgroundColor: Colors.grey,
      body: DefaultTabController(
          length: 4,
          child: TabBarView(
            children: [
              CustomScrollView(
                slivers: [
                  showSliverAppBar("Stats"),
                  SliverList(
                    delegate: SliverChildListDelegate(
                      [
                        Text(character.name + "'s statistics", textAlign: TextAlign.center),
                        Divider(),
                        Text("Main Attributes", textAlign: TextAlign.center),
                        Padding(
                          padding: const EdgeInsets.all(8.0),
                          child: Table(
                            children: [
                              TableRow(children: [
                                Column(
                                  children: [Text("Body: " + character.attributes.body.toString())],
                                ),
                                Column(
                                  children: [Text("Mind: " + character.attributes.mind.toString())],
                                ),
                                Column(
                                  children: [Text("Spirit: " + character.attributes.spirit.toString())],
                                ),
                              ]),
                              TableRow(children: [
                                Column(
                                  children: [Text("Strength: " + character.attributes.strength.toString())],
                                ),
                                Column(
                                  children: [Text("Agility: " + character.attributes.agility.toString())],
                                ),
                                Column(
                                  children: [Text("Finesse: " + character.attributes.finesse.toString())],
                                ),
                              ]),
                              TableRow(children: [
                                Column(
                                  children: [Text("Precision: " + character.attributes.precision.toString())],
                                ),
                                Column(
                                  children: [Text("Calculation: " + character.attributes.calculation.toString())],
                                ),
                                Column(
                                  children: [Text("Intuition: " + character.attributes.intuition.toString())],
                                ),
                              ]),
                              TableRow(children: [
                                Column(
                                  children: [Text("Resonance: " + character.attributes.resonance.toString())],
                                ),
                                Column(
                                  children: [Text("Projection: " + character.attributes.projection.toString())],
                                ),
                                Column(
                                  children: [Text("Poise: " + character.attributes.poise.toString())],
                                ),
                              ]),
                            ],
                          ),
                        ),
                        Divider(),
                        Row(
                          children: [
                            Text(("Character Class: ")),
                            DropdownButton(
                              value: selectedValue,
                              icon: Icon(Icons.keyboard_arrow_down),
                              hint: Text("Character select"),
                              items: [
                                DropdownMenuItem(value: 1, child: Text("Operative")),
                                DropdownMenuItem(value: 2, child: Text("Specialist")),
                                DropdownMenuItem(value: 3, child: Text("Psion")),
                              ],
                              onChanged: (value) {
                                switch (value) {
                                  case 1:
                                    character.characterClass = null;
                                    setState(() {
                                      selectedValue = value;
                                      character.characterClass = Class([], "Operative");
                                      character.characterClass.features.add(
                                          Features("At CT1, the Operative gains +2 Movement Range while wearing light armour or +1 base defence while wearing medium or heavy armour. ", "Fortitude"));
                                      character.characterClass.features.add(Features(
                                          "At CT2, the Operative is able to 'mark' an enemy for the rest of the party. This mark lasts until the start of the end of the next round and while it is active, members of the party gain a +1 to their Finesse when attacking that specific target. The Operative can use this a number of times, per 24 hours, equal to their FINESSE. ",
                                          "Combat Focus"));
                                      character.characterClass.features.add(Features(
                                          "At CT3, the Operative may force an enemy to re-roll their attack against them. They may use this Feature an amount of times equal to their Agility per 24 hours.",
                                          "Lucky Dodge"));
                                      character.characterClass.features.add(Features(
                                          "At CT4, the Operative chooses one Sub-Attribute: Strength, Agility or Finesse. The Operative treats all of the skill checks which use that Sub-Attribute as 1 skill rank higher.",
                                          "Extreme Conditioning"));
                                      character.characterClass.features.add(Features("At CT5, damage dealt to the Operative is decreased by (BODY / 2), rounded up.", "Thick Hide"));
                                    });
                                    break;
                                  case 2:
                                    character.characterClass = null;
                                    setState(() {
                                      selectedValue = value;
                                      character.characterClass = Class([], "Specialist");
                                      character.characterClass.features.add(
                                          Features("At CT1, the Specialist learns how to pack more items within their Armour’s pockets. They gain 50% more pocket space (rounded up).", "Pack Rat"));
                                      character.characterClass.features.add(Features(
                                          "At CT2, once per 24 hours, the specialist may roll twice for a skill check and add half of the second skill check to their result (rounded up). ",
                                          "Strange Luck"));
                                      character.characterClass.features.add(Features(
                                          "At CT3, the Specialist learns how to effectively repair weapons and armour back up to their base quality. Repairing weapons and armour costs 50% of their crafting cost.",
                                          "Jury-Rigger"));
                                      character.characterClass.features.add(Features(
                                          "At CT4, the Specialist  chooses one Sub-Attribute: Precision, Calculation or Intuition. The Specialist treats all of the skill checks which use that Sub-Attribute as 1 skill rank higher.",
                                          "Extreme Conditioning"));
                                      character.characterClass.features.add(Features(
                                          "At CT5, the Specialist is able to overcharge their weapons increasing the damage by 50% (Rounded up), but costing triple the usual amount of power. This Feature has a cool-down of three rounds.",
                                          "Conduit"));
                                    });
                                    break;
                                  case 3:
                                    character.characterClass = null;
                                    setState(() {
                                      selectedValue = value;
                                      character.characterClass = Class([], "Psion");
                                      character.characterClass.features
                                          .add(Features("At CT1, Psions gain +2 to their Poise when defending against Fracture Point damage from another Psion.", "Psionic Layers"));
                                      character.characterClass.features.add(Features(
                                          "At CT2, the Psion is able to move a number of Fracture Points from a living target they are touching to themselves equal to their POISE. This target must be willing. This ability can be an amount equal to the Spell-caster's Poise per encounter.",
                                          "Overflow"));
                                      character.characterClass.features.add(Features(
                                          "At CT3, the Psion learns to harness the Metaphysical energies surrounding them. The Psion gains a temporary energy shield equal to their (Poise * 2). The energy shield blocks Ballistics and Energy damage, and once empty, it takes two hours to recharge.",
                                          "Energetic Barrier"));
                                      character.characterClass.features.add(Features(
                                          "At CT4, the Psion chooses one Sub-Attribute: Resonance, Projection or Poise. The Specialist treats all of the skill checks which use that Sub-Attribute as 1 skill rank higher.",
                                          "Extreme Conditioning"));
                                      character.characterClass.features.add(Features(
                                          "At CT5, the Psion gains the ability to supercharge themselves with background Psionic energies. Once per encounter, upon using this Feature, the action cost and Fracture Point cost for their spells are reduced by half (rounded down). Once used, this Feature lasts for two rounds.",
                                          "Chaotic Overcharge"));
                                    });
                                    break;
                                }
                              },
                            ),
                          ],
                        ),
                        Row(
                          children: [Text("Character Tier: " + character.tier.toString())],
                        ),
                        Divider(),
                        Row(
                          children: [
                            Text("Max Health: " + character.health.toString()),
                            Spacer(),
                            Text("Max Mana: " + character.mana.toString()),
                            Spacer(),
                            Text("Max Fracture Points: " + character.fracturePoints.toString())
                          ],
                        ),
                      ],
                    ),
                  )
                ],
              ),
              CustomScrollView(
                slivers: [
                  showSliverAppBar("Equipment"),
                  SliverList(
                      delegate: SliverChildListDelegate([
                    Text("Character Equipment", textAlign: TextAlign.center),
                    Divider(),
                    ListTile(
                        leading: Icon(Icons.add),
                        title: Text("Add Equipment"),
                        onTap: () async {
                          Get.toNamed("/Equipment_Sheet", arguments: character);
                        }),
                    Divider(),
                    (character.equipment.length > 0)
                        ? ListView.builder(
                            shrinkWrap: true,
                            itemCount: character.equipment.length,
                            itemBuilder: (context, i) {
                              return character.equipment[i].render();
                            })
                        : Text("Your character doesn't own any equipment. \n Add some equipment onto your character.", textAlign: TextAlign.center),
                  ]))
                ],
              ),
              CustomScrollView(
                slivers: [
                  showSliverAppBar("Features & Talents"),
                  SliverList(
                      delegate: SliverChildListDelegate([
                    Text("This is the Features & Talents Page", textAlign: TextAlign.center),
                    Divider(),
                    Text("Species Features"),
                    (character.species.features.length > 0)
                        ? ListView.builder(
                            physics: NeverScrollableScrollPhysics(),
                            shrinkWrap: true,
                            itemCount: character.species.features.length,
                            itemBuilder: (context, i) {
                              return character.species.features[i].render();
                            })
                        : Text("Your character doesn't own any Species Features. \n Please choose a species.", textAlign: TextAlign.center),
                    Divider(),
                    Text("Class Features"),
                    (character.characterClass.features.length > 0)
                        ? ListView.builder(
                            physics: NeverScrollableScrollPhysics(),
                            shrinkWrap: true,
                            itemCount: character.characterClass.features.length,
                            itemBuilder: (context, i) {
                              return character.characterClass.features[i].render();
                            })
                        : Text("Your character doesn't own any Class Features. \n Please choose a Class.", textAlign: TextAlign.center),
                    Divider(),
                    Text("Character Talents"),
                    (character.talent.length > 0)
                        ? ListView.builder(
                            physics: NeverScrollableScrollPhysics(),
                            shrinkWrap: true,
                            itemCount: character.talent.length,
                            itemBuilder: (context, i) {
                              return character.talent[i].render();
                            })
                        : Text("Your character doesn't own any Character Talents. \n Please choose some Talents.", textAlign: TextAlign.center),
                  ]))
                ],
              ),
              CustomScrollView(
                slivers: [
                  showSliverAppBar("Psionic Spells"),
                  SliverList(
                      delegate: SliverChildListDelegate([
                    Text("Psionic Spells"),
                    Divider(),
                    (character.spells.length > 0)
                        ? ListView.builder(
                            physics: NeverScrollableScrollPhysics(),
                            shrinkWrap: true,
                            itemCount: character.spells.length,
                            itemBuilder: (context, spell) {
                              return character.spells[spell].render();
                            })
                        : Text("Your character doesn't own any Spells. \n Select some Spells.", textAlign: TextAlign.center),
                  ]))
                ],
              ),
            ],
          )),
    );
  }
}
