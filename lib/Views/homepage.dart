import 'dart:ui';
import 'package:flutter/material.dart';
import 'package:get/get.dart';
import 'package:shifting_tides_rulebook/Controller/characterController.dart';
import 'package:shifting_tides_rulebook/Objects/character.dart';

class HomePage extends StatefulWidget {
  HomePage({Key key}) : super(key: key);

  _HomePageState createState() => _HomePageState();
}

class _HomePageState extends State<HomePage> {
  CharacterController _characterController = new CharacterController();
  List<Character> _characters = [];
  Character selectedCharacter;

  Character _newCharacter;

  @override
  void initState() {
    super.initState();
  }

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      backgroundColor: Colors.grey,
      body: Container(
        constraints: BoxConstraints.expand(),
        decoration: BoxDecoration(image: DecorationImage(image: NetworkImage("https://flutter-examples.com/wp-content/uploads/2020/02/dice.jpg"), fit: BoxFit.cover)),
        child: Center(
          child: ClipRect(
            child: BackdropFilter(
              filter: ImageFilter.blur(sigmaX: 5, sigmaY: 5),
              child: Column(
                mainAxisAlignment: MainAxisAlignment.spaceEvenly,
                children: <Widget>[
                  Container(
                    child: Text("Shifting Tides", style: TextStyle(fontSize: 40),),
                  ),
                  Container(
                      width: 300,
                      height: 400,
                      decoration: BoxDecoration(
                        border: Border.all(
                          color: Colors.black,
                        ),
                        color: Colors.grey,
                        borderRadius: BorderRadius.circular(30),
                      ),
                      child: DefaultTabController(
                          length: 3,
                          child: Column(
                            mainAxisSize: MainAxisSize.max,
                            children: <Widget>[
                              Container(
                                decoration: BoxDecoration(
                                  border: Border.all(
                                    color: Colors.transparent,
                                  ),
                                  borderRadius: BorderRadius.circular(30),
                                ),
                                child: TabBar(
                                  tabs: [
                                    Tab(text: "Character"),
                                    Tab(text: "Rule Book"),
                                    Tab(text: "Settings"),
                                  ],
                                ),
                              ),
                              Container(
                                height: 300,
                                child: TabBarView(
                                  children: [
                                    Container(
                                      child: Center(
                                        child: Column(
                                          children: [
                                            Visibility(
                                              visible: _characters.length > 0,
                                              child: Column(
                                                children: [
                                                  Padding(
                                                    padding: const EdgeInsets.only(bottom: 8.0),
                                                    child: Text("Choose a character from the list below"),
                                                  ),
                                                  DropdownButton(
                                                    value: selectedCharacter,
                                                    icon: Icon(Icons.keyboard_arrow_down),
                                                    hint: Text("Character select"),
                                                    items: _characters.map(
                                                          (Character items) {
                                                        return DropdownMenuItem(value: items, child: Text(items.name));
                                                      },
                                                    ).toList(),
                                                    onChanged: (Character _character) {
                                                      setState(() {
                                                        selectedCharacter = _character;
                                                      });
                                                    },
                                                  ),
                                                  Container(
                                                      child: ElevatedButton(
                                                        child: Text("Open selected character's sheet"),
                                                        onPressed: () {
                                                          Get.toNamed('/Character_Sheet', arguments: selectedCharacter);
                                                        },
                                                      )),
                                                ],
                                              ),
                                            ),
                                            Text("Create a new Character"),
                                            Container(
                                                child: ElevatedButton(
                                              child: Text("Create new character"),
                                              onPressed: () {
                                                setState(() {
                                                  _newCharacter = _characterController.createCharacter();
                                                  _characters.add(_newCharacter);
                                                });
                                                Get.toNamed('/Character_Sheet', arguments: _newCharacter);
                                              },
                                            )),
                                          ],
                                        ),
                                      ),
                                    ),
                                    Container(
                                      child: Center(
                                        child: Column(
                                          children: [
                                            Text("This is the rulebook page"),
                                          ],
                                        ),
                                      ),
                                    ),
                                    Container(
                                      child: Center(
                                        child: Column(
                                          children: [
                                            Text("This is the settings page"),
                                          ],
                                        ),
                                      ),
                                    ),
                                  ],
                                ),
                              )
                            ],
                          ))),
                  Container(
                    child: Text("Info"),
                  ),
                ],
              ),
            ),
          ),
        ),
      ),
    );
  }
}
