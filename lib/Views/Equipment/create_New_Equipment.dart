import 'package:find_dropdown/find_dropdown.dart';
import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';
import 'package:get/get_core/src/get_main.dart';
import 'package:get/get.dart';
import 'package:shifting_tides_rulebook/Controller/equipmentController.dart';
import 'package:shifting_tides_rulebook/Objects/character.dart';
import 'package:shifting_tides_rulebook/Objects/equipment.dart';

class EquipmentSheet extends StatefulWidget {
  EquipmentSheet() : super();

  _EquipmentSheetState createState() => _EquipmentSheetState();
}

class _EquipmentSheetState extends State<EquipmentSheet> with SingleTickerProviderStateMixin {
  // Controllers
  EquipmentController _equipmentController = new EquipmentController();

  // Initializer
  Character character = Get.arguments;
  Weapon tempWeapon;
  Armour tempArmour;
  List<Armour> armourList;

  // Variables
  int itemSelection = 1, weaponTypeSelection = 1, weaponDamageSelection = 1, weaponRarity = 1, armourType = 1, armour = 1;
  bool weaponSwitch = true, armourSwitch = false, shieldSwitch = false, itemSwitch = false, stimulantSwitch = false;

  @override
  void initState() {
    tempWeapon = _equipmentController.createWeapon();
    tempArmour = _equipmentController.createArmour();
    armourList = _equipmentController.listArmours();
    super.initState();
  }

  @override
  void dispose() {
    super.dispose();
  }

  SliverAppBar showSliverAppBar(String screenTitle) {
    return SliverAppBar(
      backgroundColor: Colors.purple,
      floating: true,
      pinned: true,
      snap: false,
      title: Text(screenTitle),
    );
  }

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      backgroundColor: Colors.grey,
      body: CustomScrollView(
        slivers: [
          showSliverAppBar("Create new Equipment"),
          SliverList(
            delegate: SliverChildListDelegate(
              [
                Padding(
                  padding: const EdgeInsets.all(8.0),
                  child: Container(
                    height: MediaQuery.of(context).size.height,
                    child: ListView(
                      shrinkWrap: true,
                      children: [
                        Text("Item Type: "),
                        DropdownButton(
                          value: itemSelection,
                          items: [
                            DropdownMenuItem(value: 1, child: Text("Weapon")),
                            DropdownMenuItem(value: 2, child: Text("Armour")),
                            DropdownMenuItem(value: 3, child: Text("Shield")),
                            DropdownMenuItem(value: 4, child: Text("Item")),
                            DropdownMenuItem(value: 5, child: Text("Stimulant")),
                          ],
                          onChanged: (value) {
                            setState(() {
                              switch (value) {
                                case 1: // Weapon
                                  itemSelection = value;
                                  weaponSwitch = true;
                                  armourSwitch = false;
                                  shieldSwitch = false;
                                  itemSwitch = false;
                                  stimulantSwitch = false;
                                  break;
                                case 2: // Armour
                                  itemSelection = value;
                                  weaponSwitch = false;
                                  armourSwitch = true;
                                  shieldSwitch = false;
                                  itemSwitch = false;
                                  stimulantSwitch = false;
                                  break;
                                case 3: // Shield
                                  itemSelection = value;
                                  weaponSwitch = false;
                                  armourSwitch = false;
                                  shieldSwitch = true;
                                  itemSwitch = false;
                                  stimulantSwitch = false;
                                  break;
                                case 4: // Item
                                  itemSelection = value;
                                  weaponSwitch = false;
                                  armourSwitch = false;
                                  shieldSwitch = false;
                                  itemSwitch = true;
                                  stimulantSwitch = false;
                                  break;
                                case 5: // Stimulant
                                  itemSelection = value;
                                  weaponSwitch = false;
                                  armourSwitch = false;
                                  shieldSwitch = false;
                                  itemSwitch = false;
                                  stimulantSwitch = true;
                                  break;
                              }
                            });
                          },
                        ),
                        // Item visibilities
                        Visibility(
                            visible: weaponSwitch,
                            child: Column(children: [
                              Row(
                                children: [
                                  Text("Item Rarity: "),
                                  DropdownButton(
                                      value: weaponRarity,
                                      items: [
                                        DropdownMenuItem(value: 1, child: Text("Mundane")),
                                        DropdownMenuItem(value: 2, child: Text("Silver")),
                                        DropdownMenuItem(value: 3, child: Text("Gold")),
                                      ],
                                      onChanged: (value) {
                                        setState(() {
                                          switch (value) {
                                            case 1:
                                              tempWeapon.rarity = 1;
                                              tempWeapon.rarityColor = Colors.red[200];
                                              weaponRarity = value;
                                              break;
                                            case 2:
                                              tempWeapon.rarity = 2;
                                              tempWeapon.rarityColor = Colors.grey[600];
                                              weaponRarity = value;
                                              break;
                                            case 3:
                                              tempWeapon.rarity = 3;
                                              tempWeapon.rarityColor = Colors.yellow[700];
                                              weaponRarity = value;
                                              break;
                                          }
                                        });
                                      })
                                ],
                              ),
                              Row(
                                children: [
                                  Text("Weapon Attribute: "),
                                  DropdownButton(
                                    value: weaponTypeSelection,
                                    items: [
                                      DropdownMenuItem(value: 1, child: Text("Strength")),
                                      DropdownMenuItem(value: 2, child: Text("Agility")),
                                      DropdownMenuItem(value: 3, child: Text("Finesse")),
                                    ],
                                    onChanged: (value) {
                                      setState(() {
                                        switch (value) {
                                          case 1:
                                            weaponTypeSelection = value;
                                            tempWeapon.attributeType = "Strength";
                                            tempWeapon.damage = "3d4";
                                            break;
                                          case 2:
                                            weaponTypeSelection = value;
                                            tempWeapon.attributeType = "Agility";
                                            tempWeapon.damage = "1d4";
                                            break;
                                          case 3:
                                            weaponTypeSelection = value;
                                            tempWeapon.attributeType = "Finesse";
                                            tempWeapon.damage = "1d6 / 2d4";
                                            break;
                                        }
                                      });
                                    },
                                  ),
                                ],
                              ),
                              Row(
                                children: [
                                  Text("Damage Type: "),
                                  DropdownButton(
                                    value: weaponDamageSelection,
                                    items: (weaponTypeSelection == 2)
                                        ? [
                                            DropdownMenuItem(value: 1, child: Text("Bladed")),
                                            DropdownMenuItem(value: 2, child: Text("Blunt")),
                                            DropdownMenuItem(value: 3, child: Text("Pointed")),
                                            DropdownMenuItem(value: 4, child: Text("Pointed - Fist")),
                                          ]
                                        : (weaponTypeSelection == 3)
                                            ? [
                                                DropdownMenuItem(value: 1, child: Text("Bladed")),
                                                DropdownMenuItem(value: 3, child: Text("Blunt")),
                                                DropdownMenuItem(value: 4, child: Text("Point")),
                                                DropdownMenuItem(value: 2, child: Text("Bladed - Fist")),
                                              ]
                                            : [
                                                DropdownMenuItem(value: 1, child: Text("Blade")),
                                                DropdownMenuItem(value: 2, child: Text("Blunt")),
                                                DropdownMenuItem(value: 3, child: Text("Pointed")),
                                              ],
                                    onChanged: (value) {
                                      setState(() {
                                        switch (value) {
                                          case 1:
                                            weaponDamageSelection = value;
                                            tempWeapon.type = "Bladed";
                                            break;
                                          case 2:
                                            weaponDamageSelection = value;
                                            tempWeapon.type = "Blunt";
                                            break;
                                          case 3:
                                            weaponDamageSelection = value;
                                            tempWeapon.type = "Pointed";
                                            break;
                                          case 4:
                                            weaponDamageSelection = value;
                                            if (weaponTypeSelection == 2) {
                                              tempWeapon.type = "Pointed - Fist";
                                            } else if (weaponTypeSelection == 3) {
                                              tempWeapon.type = "Bladed - Fist";
                                            }
                                            break;
                                        }
                                      });
                                    },
                                  ),
                                ],
                              ),
                              Row(
                                children: [
                                  TextButton(
                                    style: TextButton.styleFrom(
                                      primary: Colors.white,
                                      backgroundColor: Colors.teal,
                                      onSurface: Colors.grey,
                                    ),
                                    child: Text("Generate Weapon modifiers"),
                                    onPressed: () {
                                      setState(() {
                                        tempWeapon.modifiers.prefix = [];
                                        tempWeapon.modifiers.suffix = [];
                                        if (tempWeapon.rarity == 2) {
                                          tempWeapon.modifiers.prefix.add("Test - Prefix");
                                        }
                                        if (tempWeapon.rarity == 3) {
                                          tempWeapon.modifiers.prefix.add("Test - Prefix");
                                          tempWeapon.modifiers.suffix.add("Test- Suffix");
                                        }
                                      });
                                    },
                                  ),
                                ],
                              ),
                              tempWeapon.render(),
                              Row(
                                mainAxisAlignment: MainAxisAlignment.spaceAround,
                                children: [
                                  TextButton(
                                    style: TextButton.styleFrom(
                                      primary: Colors.white,
                                      backgroundColor: Colors.teal,
                                      onSurface: Colors.grey,
                                    ),
                                    child: Text("Add Item to Inventory"),
                                    onPressed: () {
                                      setState(() {
                                        character.equipment.add(tempWeapon);
                                      });
                                      Get.toNamed("/Character_Sheet", arguments: character);
                                    },
                                  ),
                                ],
                              ),
                            ])),
                        Visibility(
                            visible: armourSwitch,
                            child: Column(children: [
                              Row(
                                children: [
                                  Text("Armour: "),
                                ],
                              ),
                              Container(
                                height: 500,
                                child: ListView.builder(
                                    shrinkWrap: true,
                                    itemCount: armourList.length,
                                    itemBuilder: (BuildContext context, int index) {
                                      return GestureDetector(
                                        child: armourList[index].render(),
                                        onTap: () {
                                          tempArmour = armourList[index];
                                        },
                                      );
                                    }),
                              ),
                              Container(
                                child: Row(
                                  mainAxisAlignment: MainAxisAlignment.spaceAround,
                                  children: [
                                    TextButton(
                                      style: TextButton.styleFrom(
                                        primary: Colors.white,
                                        backgroundColor: Colors.teal,
                                        onSurface: Colors.grey,
                                      ),
                                      child: Text("Add Item to Inventory"),
                                      onPressed: () {
                                        setState(() {
                                          character.equipment.add(tempArmour);
                                        });
                                        Get.toNamed("/Character_Sheet", arguments: character);
                                      },
                                    ),
                                  ],
                                ),
                              ),
                            ])),
                        Visibility(
                            visible: shieldSwitch,
                            child: Column(children: [
                              Row(
                                children: [],
                              )
                            ])),
                        Visibility(
                            visible: itemSwitch,
                            child: Column(children: [
                              Row(
                                children: [],
                              )
                            ])),
                        Visibility(
                            visible: stimulantSwitch,
                            child: Column(children: [
                              Row(
                                children: [],
                              )
                            ])),
                      ],
                    ),
                  ),
                )
              ],
            ),
          )
        ],
      ),
    );
  }
}
